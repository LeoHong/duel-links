import React from 'react';

import './index.css';

const Preview = React.memo(props => {
  const { milestones } = props;
  
  const getMilestoneCard = (key, milestone) => {
    return (
      <li key={key} className='card'>
        <h2 className='name'>{milestone.title}</h2>
        <h3 className='porfolio'>{milestone.portfolio}</h3>
        <div className='tag'>{milestone.phase.tag}</div>
        <pre className='description'>{milestone.description}</pre>
        <div className='meta'>{`Posted by ${milestone.author} on ${milestone.date}`}</div>
      </li>
    )
  }

  return (
    <div className='preview-wrapper'>
      <ul>
        {milestones.map((milestone, k) => {
          return getMilestoneCard(k, milestone)
        })}
      </ul>
    </div>
  )
});

export default Preview;