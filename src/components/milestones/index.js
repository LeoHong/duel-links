import React from 'react';

import './index.css';


const Milestones = React.memo(props => {
  const { milestones } = props;
  
  const getMilestoneCard = (key, milestone) => {
    return (
      <li key={key} className='card'>
        <div className='name'>{milestone.title}</div>
        <div className='date'>{milestone.date}</div>
        <div className='porfolio'>{milestone.portfolio}</div>
        <div 
          className='tag'
          style={{'backgroundColor': milestone.phase.color}}
        >
          {milestone.phase.tag}
        </div>
      </li>
    )
  }

  return (
    <div className='milestone-wrapper'>
      <ul>
        {milestones.map((milestone, k) => {
          return getMilestoneCard(k, milestone)
        })}
      </ul>
    </div>
  )
});

export default Milestones;