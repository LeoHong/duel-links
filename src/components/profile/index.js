import React from 'react';
import { Icon } from 'antd';

import './index.css';


const Profile = React.memo(props => {
  const openNewPage = (url) => {
    window.open(url);
  }

  const getContactList = () => {
    return (
      <ul className='contact-list'>
        <li onClick={() => openNewPage('https://www.linkedin.com/in/leyou-hong')}>
          <Icon type='linkedin'/>
        </li>
        <li onClick={() => openNewPage('https://www.facebook.com/leyou.hong')}>
          <Icon type='facebook'/>
        </li>
        <li onClick={() => openNewPage('https://github.com/LeyouHong')}>
          <Icon type='github'/>
        </li>
        <li onClick={() => openNewPage('https://leetcode.com/hongleyou')}>
          <div className='leetcode-icon'></div>
        </li>
      </ul>
    )
  }

  return (
    <div className='profile-wrapper'>
      <h5>ABOUT ME</h5>
      <div className='img'></div>
      <p>
        The study and creation is the life two feet
        <br/>
        学习与创造是人生的两只脚
      </p>
      { getContactList() }
      <div className='email'>leyouhong1991@gmail.com</div>
    </div>
  )
});

export default Profile;