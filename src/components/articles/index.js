import React, { useState } from 'react';
import { Table, Drawer } from 'antd';

import './index.css';
import isSmallScreen from '../../common/function/isSmallScreen';


const Article = React.memo(props => {
  let [showDrawerByTitle, setShowDrawerByTitle] = useState({});

  const getSmallScreenTable = () => {
    const columns = [
      {
        title: 'article name',
        dataIndex: 'title',
        key: 'title'
      },
      {
        title: 'author',
        dataIndex: 'author',
        key: 'author'
      }
    ];

    const viewHandler = record => {
      setShowDrawerByTitle(record)
    }

    let obj = {
      title: '',
      className: 'view',
      key: 'view',
      render: (text, record) => (
        <span onClick={e => {
          e.stopPropagation();
          viewHandler(record);
        }}>view</span>
      )
    }

    columns.push(obj);

    return (
      <Table
        className='article-table'
        onRow={record => {
          return {
            onClick: () => console.log(record)
          }
        }}
        columns={columns}
        dataSource={props.articles}
        rowKey={record => record.title}
        pagination = {{
          pageSize: 6,
          hideOnSinglePage: true}}
      />
    )
  }

  const getLargeScreenTable = () => {
    const columns = [
      {
        title: 'article name',
        dataIndex: 'title',
        key: 'title'
      },
      {
        title: 'portfolio',
        dataIndex: 'portfolio',
        key: 'portfolio'
      },
      {
        title: 'tag',
        dataIndex: 'tag',
        key: 'tag'
      },
      {
        title: 'author',
        dataIndex: 'author',
        key: 'author'
      },
      {
        title: 'create date',
        dataIndex: 'date',
        key: 'date'
      },
    ];

    return (
      <Table
        className='article-table'
        onRow={record => {
          return {
            onClick: () => console.log(record)
          }
        }}
        columns={columns}
        dataSource={props.articles}
        rowKey={record => record.title}
        pagination = {{
          pageSize: 6,
          hideOnSinglePage: true}}
      />
    )
  }

  return (
    <div className='article-wrapper'>
      { isSmallScreen() ? getSmallScreenTable() : getLargeScreenTable() }
      <Drawer
        title=""
        placement='bottom'
        closable={true}
        onClose={() => setShowDrawerByTitle({})}
        visible={showDrawerByTitle.title !== undefined}
        className='article-detail-drawer'
        height='40vh'
      >
        <div className='title'>{showDrawerByTitle.title}</div>
        <div>
          <span>
            portfolio:
            <span>{showDrawerByTitle.portfolio}</span>
          </span>
          <span>
            tag:
            <span>{showDrawerByTitle.tag}</span>
          </span>
          <span>
            author:
            <span>{showDrawerByTitle.author}</span>
          </span>
          <span>
            create-date:
            <span>{showDrawerByTitle.date}</span>
          </span>
        </div>
      </Drawer>
    </div>
  )
});

export default Article;