import React, { useState } from 'react';
import { HashRouter, Link } from 'react-router-dom';
import { Icon, Drawer } from 'antd';

import isSmallScreen from '../../common/function/isSmallScreen';

import './index.css';


const Header = React.memo(props => {
  const phases = [
    { phaseName:'Dashboard', iconName: 'dashboard' },
    { phaseName : 'Articles', iconName: 'ordered-list' },
    { phaseName : 'Users', iconName: 'usergroup-add' },
    { phaseName : 'About', iconName: 'user' }
  ];

  let [showDrawer, setShowDrawer] = useState(false);
  
  const getLargeScreenHeader = () => {
    return (
      <div id='header'>
        <div className='logo-wapper'>
          <Icon 
            className='logo'
            type='area-chart'/>
        </div>
        <ul>
        { phases.map((v,k) => {
          return (
            <li key={k} className={v.phaseName}>
              <Link to= {`/${v.phaseName.toLowerCase()}`}>
                {/* <Icon type={v.iconName} /> */}
                {'  ' + v.phaseName}
              </Link>
            </li>
          )
        })}
        </ul>
        <Link to= {'/login'}>
          <Icon className='login' type='user'/>
        </Link>
      </div>
    )
  }

  const onCloseDrawer = () => {
    setShowDrawer(false);
  }

  const getSmallScreenHeader = () => {
    return (
      <div id='header'>
        <Icon 
          className='header-menu'
          type={showDrawer ? 'close' : 'menu'}
          onClick={() => setShowDrawer(!showDrawer)}
        />
        <Link to= {'/login'}>
          <Icon className='header-user' type='user' />
        </Link>
        <Drawer
          title=""
          placement='top'
          closable={false}
          onClose={onCloseDrawer}
          visible={showDrawer}
          className='nav-drawer'
          height='calc(100vh - 48px)'
        >
          <ul>
            { phases.map((v,k) => {
              return (
                <li key={k} className={v.phaseName}>
                  <Link 
                    to= {`/${v.phaseName.toLowerCase()}`}
                    onClick={() => setShowDrawer(false)}
                  >
                    {'  ' + v.phaseName}
                  </Link>
                </li>
              )})}
          </ul>
        </Drawer>
      </div>
    )
  }

  return(
    <HashRouter>
      { isSmallScreen() ?
        getSmallScreenHeader() : getLargeScreenHeader() }
    </HashRouter>
  )
});

export default Header;
