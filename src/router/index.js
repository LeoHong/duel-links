import React from 'react';
import { HashRouter, Switch, Route, Redirect } from 'react-router-dom';

import Dashboard from '../container/dashboard';
import ArticleSetup from '../container/articlesetup';
import Usersetup from '../container/usersetup';
import About from '../container/about';
import Login from '../container/login';
import Verify from '../container/verify';


const Router = React.memo(props => {
  return (
    <HashRouter>
      <Switch>
        <Route exact path='/'
          render = {() => {
            return (<Redirect to='/dashboard'/>)
        }}/>
        <Route exact path='/dashboard' component={Dashboard}/>
        <Route exact path='/articles' component={ArticleSetup}/>
        <Route exact path='/users' component={Usersetup}/>
        <Route exact path='/about' component={About}/>
        <Route exact path='/login' component={Login}/>
        <Route exact path='/verify' component={Verify}/>
      </Switch>
    </HashRouter>
  )
});

export default Router;