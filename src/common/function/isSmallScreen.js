function isSmallScreen() {
  const match = window.matchMedia("(min-width: 320px) and (max-width: 599px)");
  
  return match && match.matches;
}

export default isSmallScreen;