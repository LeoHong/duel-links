import React from 'react';
import { Provider } from 'react-redux';

import store from './store/index';
import Router from './router';
import Header from './components/header';

import './App.css';


const App = React.memo(props => {
  return (
    <Provider store={store}>
      <div className='App'>
        <Header />
        <Router match={props.match}/>
      </div>
    </Provider>
  );
});

export default App;
