import { combineReducers } from 'redux-immutable';
import dashboard from '../container/dashboard/store/reducer';
import articlesetup from '../container/articlesetup/store/reducer';
import about from '../container/about/store/reducer';

const reducer = combineReducers({
  dashboard,
  articlesetup,
  about
})

export default reducer;
