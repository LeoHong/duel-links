import React from 'react';
import { Link } from 'react-router-dom';
import { Form, Input, Button } from 'antd';

import './index.css';


const Login = React.memo(props => {
  return (
    <div className='login-wrapper'>
      <div className='login-header'>Please sign in.</div>
      <div className='login-container'>
        <Form
          name='login'
          initialvalues={{
            remember: true,
          }}
          // onFinish={onFinish}
          // onFinishFailed={onFinishFailed}
          >
          <Form.Item
            label="Email"
            name="email"
            rules={[
              {
                required: true,
                message: 'Please input your email!',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Password"
            name="password"
            rules={[
              {
                required: true,
                message: 'Please input your password!',
              },
            ]}
          >
            <Input.Password />
          </Form.Item>

          <div className='sign-info'>
            Your account is the email address you use to sign in and manage your articles.  
          </div>  
          <Button type="primary" htmlType="submit">
            <span>Sign in</span>
          </Button>
          <Link to= {'/verify'}><div className='forgot'>Forgot your Account or password?</div></Link>
          <Link to= {'/login'}><div className='create'>Don't have an Account? Create one now.</div></Link>
        </Form>
      </div>
    </div>
  )
});

export default Login;