import { actionTypes } from './index';
import axios from 'axios';
// import { message } from 'antd';

export const getAllArticles = () => {
  return dispatch => {
    axios.get('/api/articles.json').then(res => {
      dispatch(getAllArticlesAction(res.data));
    }).catch(err => { alert("Fail to get articles") })
  }
}

export const getAllArticlesAction = allArticles => ({
  type: actionTypes.GET_ALL_ARTICLES, 
  allArticles
})