import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Input } from 'antd';

import { actionCreators } from './store';
import Ariticles from '../../components/articles';

import './index.css';

const { Search } = Input;


const ArticleSetup = React.memo(props => {
  useEffect(()=>{
    props.getAllArticles();
  }, []);

  let [searchValue, setSearchValue] = useState('');

  const filterArticles = () => {
    if (!props.articles) return [];

    return props.articles.filter(article => {
      return article.title.toLowerCase().includes(searchValue) ||
             article.portfolio.toLowerCase().includes(searchValue) ||
             article.tag.toLowerCase().includes(searchValue) ||
             article.author.toLowerCase().includes(searchValue) ||
             article.date.toLowerCase().includes(searchValue);
    });
  }

  return (
    <div className='articlesetup'>
      <div className='title-content'>
        <div className='left'>Ariticles</div>
        <div className='right'>
          <Search 
            placeholder='Search'
            onSearch={value => setSearchValue(value.toLowerCase())}
          />
        </div>
      </div>
      <Ariticles articles={filterArticles()}/>
    </div>
  )
});

const mapStateToProps = state => {
  return {
    articles: state.get('articlesetup').get('articles')
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getAllArticles() {
      dispatch(actionCreators.getAllArticles())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ArticleSetup);