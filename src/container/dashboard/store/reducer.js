import { fromJS } from 'immutable';
import { actionTypes } from './index';

const defaultState = fromJS({
  tabName: 'overview'
});

export default (state = defaultState, action) => {
  switch(action.type) {
    case actionTypes.GET_ALL_MILESTONES:
      return state.set('milestones', action.allMilestones);
    default:
      return state;
  }
}