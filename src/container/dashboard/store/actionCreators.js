import { actionTypes } from './index';
import axios from 'axios';
// import { message } from 'antd';

export const getAllMilestones = () => {
  return dispatch => {
    axios.get('/api/milestones.json').then(res => {
      // console.log(res)
      dispatch(getAllMilestonesAction(res.data));
    }).catch(err => { alert("Fail to get milestones") })
  }
}

export const getAllMilestonesAction = allMilestones => ({
  type: actionTypes.GET_ALL_MILESTONES, 
  allMilestones
})