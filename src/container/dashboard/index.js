import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { actionCreators } from './store';
import Milestones from '../../components/milestones';
import './index.css';

const Dashboard = React.memo(props => {
  useEffect(()=>{
    props.getAllMilestones();
  }, []);

  return (
    <div className='dashboard'>
      <Milestones milestones={props.milestones || []} />
    </div>
  )
});

const mapStateToProps = state => {
  return {
    milestones: state.get('dashboard').get('milestones')
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getAllMilestones() {
      dispatch(actionCreators.getAllMilestones())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);