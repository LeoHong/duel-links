import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';

import Profile from '../../components/profile';
import Preview from '../../components/preview';
import { actionCreators } from './store';

import './index.css';
import isSmallScreen from '../../common/function/isSmallScreen';


const About = React.memo(props => {
  useEffect(()=>{
    props.getHotMilestones();
  }, []);

  let [milestoneIndex, setMilestoneIndex] = useState(0);
  const pageSize = 2;

  const getMilestones = () => {
    return props.milestones ? props.milestones.slice(milestoneIndex, milestoneIndex+pageSize) : [];
  }

  const getIntroduceHeader = () => {
    if (isSmallScreen()) return null;
    
    return (
      <div className='introduce-header'>
        <div className='site-heading'>
          <h1>Leyou Hong</h1>
          <div>Thinking will not overcome fear but action will.</div>
        </div>
      </div>
    )
  }

  const olderPostOnClickHandler = () => {
    if (props.milestones && milestoneIndex+pageSize < props.milestones.length-1) {
      setMilestoneIndex(milestoneIndex+pageSize); 
    }
  }

  return (
    <div className='aboutme'>
      { getIntroduceHeader() }
      <div className='aboutme-container'>
        <div className='left-part'>
          <Preview milestones={ getMilestones() } />
          <div 
            className='older-post'
            onClick={olderPostOnClickHandler}>
            OLDER POST →
          </div>
        </div>
        <div className='right-part'>
          <Profile />
        </div>
      </div>
    </div>
  )
});

const mapStateToProps = state => {
  return {
    milestones: state.get('about').get('milestones')
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getHotMilestones() {
      dispatch(actionCreators.getHotMilestones())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(About);