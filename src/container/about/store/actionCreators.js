import { actionTypes } from './index';
import axios from 'axios';
// import { message } from 'antd';

export const getHotMilestones = () => {
  return dispatch => {
    axios.get('/api/hotmilestones.json').then(res => {
      dispatch(getHotMilestonesAction(res.data));
    }).catch(err => { alert("Fail to get milestones") })
  }
}

export const getHotMilestonesAction = hotMilestones => ({
  type: actionTypes.GET_HOT_MILESTONES, 
  hotMilestones
})