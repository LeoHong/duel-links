import React, { useState } from 'react';
import { Form, Input, Button, Icon } from 'antd';
import { Link } from 'react-router-dom';

import './index.css';


const Verify = React.memo(props => {
  let [inputValue, setInputValue] = useState('');
  let [needResetPassword, setNeedResetPassword] = useState(false);

  const onChangeHandler = e => {
    setInputValue(e.target.value);
  }

  const getEncodeEmail = () => {
    const first = inputValue[0]+'•••••@';
    const second = inputValue.split('@')[1][0] + '•••••.';
    const third = inputValue.split('.')[1];

    return first+second+third;
  }

  const getResetSuccess = () => {
    return (
      <div className='content-main'>
        <div className='success-icon-wrap'>
          <Icon type='check-circle' />
        </div>
        <p className='title'>Password Reset Email Sent</p>
        <p className='description'>
          An email has been sent to your email address, <strong>{getEncodeEmail()}</strong>. Follow the directions in the email to reset your password.
        </p>
        <Link to= {'/login'}>
          <div className='button-wrapper'>
            <Button 
              type="primary"
              onClick={changeResetHandler}>
              <span>Done</span>
            </Button>
          </div>
        </Link>
      </div>
    )
  }

  const changeResetHandler = () => {
    setNeedResetPassword(true);
  }

  const checkEmail = () => {
    const reg = /^([A-Za-z0-9_\-.])+@([A-Za-z0-9_\-.])+\.([A-Za-z]{2,4})$/;
    return reg.test(inputValue);
  }

  const getVerifyWrapper = () => {
    return (
      <div className='verify-wrapper'>
        <div className='left'>
          <h1>Having trouble signing in?</h1>
          <p className='description1'>Enter your Email to reset.</p>
          <Form name='verify'>
            <Form.Item
              label="Email"
              name="email"
              rules={[
                {
                  required: true,
                  message: 'Please input your email!',
                },
              ]}
            >
              <Input onChange={onChangeHandler}/>
            </Form.Item>
            <p className='description2'>If you forgot your Email, you can't login.</p>
            <Button 
              type="primary"
              htmlType="submit"
              onClick={changeResetHandler}
              disabled={!checkEmail()}>
              <span>Continue</span>
            </Button>
          </Form>
        </div>
        <div className='right'></div>
      </div>
    )
  }

  return (
    <>
      { needResetPassword ? getResetSuccess() : getVerifyWrapper() }
    </>
  )
});

export default Verify;