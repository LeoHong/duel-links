const local = {
  app: {
    path: "/",
    api: "http://localhost:8080/v1"
  },
};

const dev = {
  app: {
    path: "/",
    api: "https://auoqf5z6c2.execute-api.us-east-1.amazonaws.com/dev/v1"
  },
};

const prod = {
  app: {
    path: "/",
    api: "https://auoqf5z6c2.execute-api.us-east-1.amazonaws.com/dev/v1"
  },
};

let config = {};

switch (process.env.REACT_APP_STAGE) {
  case "PRODUCTION":
    config = prod;
    break;

  case "DEVELOPMENT":
    config = dev;
    break;

  default:
    config = local;
    break;
}

export default {
  ...config
};
